import requests
from fake_useragent import UserAgent
import time

req = requests.Session()
ua = UserAgent()
Cookie = "Сюда вставляем Cookie" # Вставляем Cookie из файла popul
Host = "hh.ru"
UserAgent = ua.random
req.headers = {
    "Cookie": Cookie,
    "Host": Host,
    "User-Agent": UserAgent}

letter = '''Текст сопроводительного письма'''
# больше параметров на https://api.hh.ru/
params = {
    'text': 'NAME:python',  # Вакансия
    # 'area': 1,  Поиск по городам
    # 'employment': тип занятости
    # 'schedule' : график работы
    # 'salary': размер заработной платы
    # 'date_from': – дата, которая ограничивает снизу диапазон дат публикации вакансий.'
    # 'date_to': – дата, которая ограничивает сверху диапазон дат публикации вакансий
    'experience': "noExperience",
    'page': 0,  # Индекс страницы поиска на HH
    'per_page': 50,  # Кол-во вакансий на 1 странице
    'period': 10  # количество дней, в пределах которых нужно найти вакансии.(макс 30)
}

req1 = requests.get('https://api.hh.ru/vacancies', params)
b = req1.json()
dlin = len(b['items'])
if dlin == 0:
    print('Не найдено вакансий по заданным параметрам поиска. Поменяйте критерии поиска')
else:
    for i in range(dlin):
        id = b['items'][i]['id']
        print(b['items'][i]['apply_alternate_url'])
        ba = req.post('https://hh.ru/applicant/vacancy_response/', data={
        '_xsrf': '',# Вставляем _xsrf из файла popul
        'vacancy_id': id,
        'resume_hash': '',# ваш id резюме
        'ignore_postponed': True,
        'incomplete': False,
        'letter': letter,
        'lux': True,
        'withoutTest': 'no',
        'hhtmFromLabel': 'undefined',
        'hhtmSourceLabel': 'undefined'})
        time.sleep(1)
print(f"Откликнулись на {dlin} вакансий")
